function g = ggsp(n, f)
  ## Esta funcion genera un marco de parseval G basado un un marco de entrada F.
  ## Un marco de Parseval phi tiene las siguientes propiedades:
  ##
  ## * Si f_k \in span(F) entonces:
  ##     phi(f_i) = S^{-1/2}(phi(f_i), f_k)
  ##
  ##     Donde S es el operador de frame de (phi(f_i), f_k)
  ##
  ## * Si f_k \notin span(F)
  ##
  ##     phi(f_i) = (phi(f_i), g_k)
  ##
  ##     con g_k \in H, ||g_k|| = 1
  ##     y g_k \perp phi(f_i)
  ##
  ## Entrada:
  ## * Marco F
  ## * Cantidad de elementos en el marco F
  ## Salida:
  ## * Marco de Parseval F

  g = []; % Marco de Salida
  G = {}; % Contenedor con los pasos intermedios para graficar

  G(1) = [NaN; NaN]; % Placeholder para el caso inicial donde solo se muestran los puentos de entrada
  
  m = size(f)(1);

  for k = 1:n
    f_k = f(:, k);
    
    if f_k == 0
      g_k = zeros(m, 1);
    else
      g_k = f_k;
      for j = 1:(k-1)
	g_k = g_k - dot(f_k, g(:, j)) * g(:, j);
      endfor

      if sum(abs(g_k)) > 1e-12
	g_k = g_k ./ norm(g_k);
      else
	for i = 1:(k-1)
	  g(:, i) = g(:, i)...
		    + dot(g(:, i), f_k) * f_k ...
		      ./ (norm(f_k)**2) .* (1 / (sqrt(1 + norm(f_k)**2)) - 1);
	endfor
	g_k = f_k ./ sqrt(1 + norm(f_k)**2);
      endif
    endif
    
    g = horzcat(g, g_k);
    G(k+1) = g;
      
  endfor

  for i = 1:9
    subplot(3,3, i);
    g_step = cell2mat(G(1, i));

    ## Plot 2D points
    if m == 2
      scatter(f(1, :)([i+1:n]), f(2, :)([i+1:n]), 504, "r", "s");
      hold on;
      if i != 1
	scatter(g_step(1, :), g_step(2, :), 504, "b", "c");
      endif
      
      axis([-1, 1, -1, 1]);
      drawnow;
      ## Plot 3D points
    elseif m == 3
      scatter3(f(1, :)([i+1:n]), f(2, :)([i+1:n]), f(3, :)([i+1:n]), 504, "r", "s");
      hold on;
      if i != 1
	scatter3(g_step(1, :), g_step(2, :), g_step(3, :), 504, "b", "c");
      endif
      
      axis([-2, 10, -2, 10,-2, 10]);
      drawnow;
    endif
  endfor

  input("Press any key to exit");
endfunction
