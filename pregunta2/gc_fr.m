function y_j = gc_fr(phi, coeffs)
  ## Entrada:
  ## * Marco phi
  ## * Coeficientes de la senal en el marco
  ## Salida:
  ## * Señal reconstruida y
  ##
  
  S = phi * phi';  
  x = phi * coeffs; % Esto es equivalente a S * y, con y siendo la señal original

  r_j = x;
  p_j = x;
  p_oldj = zeros(size(p_j));
  y_oldj = zeros(size(p_j));
  y_j = zeros(size(p_j)) + 1e-5; % Small bias to avoid division by zero on first iteration
  j = 1;

  while (norm(y_j - y_oldj)/norm(y_j) > 1e-5)
    y_oldj = y_j;

    lambda_j = dot(r_j, p_j) / dot(p_j, S * p_j);
    y_j = y_oldj + lambda_j * p_j;
    r_j = r_j - lambda_j * S * p_j;
    p_tmp = p_j;
    
    if j != 1
      p_j = S * p_j ...
	    - dot(S * p_j, S * p_j) / dot(p_j, S * p_j) * p_j ...
	    - dot(S * p_j, S * p_oldj) / dot(p_oldj, S * p_oldj) * p_oldj;
    else
      p_j = S * p_j - dot(S * p_j, S * p_j) / dot(p_j, S * p_j) * p_j;
    endif

    p_oldj = p_tmp;
    j++;
  endwhile
endfunction
