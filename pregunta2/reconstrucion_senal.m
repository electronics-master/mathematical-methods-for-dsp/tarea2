N = 1001;
T = 1;
f_1 = 50;
a_1 = 1;
f_2 = 120;
a_2 = 2;

% Sampling points
t = linspace(0, T, N)';
y = a_1 * sin(2 * pi * f_1 * t) + a_2 * sin(2 * pi * f_2 * t);

% Load Frame
load("marco.mat");

% Coeficientes del marco
% Esto es equivalente a utilizar el operador de análisis en su forma matricial

coeffs_y = F' * y;

% Reconstruir Señal

reconstructed_y = gc_fr(F, coeffs_y);

h = figure(1);

plot(y);
hold on;
plot(reconstructed_y);

drawnow;

input("Press any key to exit");

